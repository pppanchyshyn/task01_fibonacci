package com.epam.lab.basic;

/**
 *This interface contains all constants.
 *
 * @version 1.0
 * @author Pavlo Panchyshyn
 * @since 2019-03-31
 *
 */

public interface Constants {
  int CHOICE_1 = 1;
  int CHOICE_2 = 2;
  int CHOICE_3 = 3;
  int CHOICE_4 = 4;
  int CHOICE_5 = 5;
  int CHOICE_6 = 6;
  int CHOICE_7 = 7;
  int ONE_HUNDRED_PERCENT = 100;
  int MIN_SET_SIZE = 3;
  int REWIND_VALUE = 6;
  int RESTART_VALUE = 7;
  int NUMBERS_PER_LINE = 20;
}
