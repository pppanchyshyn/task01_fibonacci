package com.epam.lab.basic;

/**
 *The main class, the entry point of the program.
 *
 * @version 1.0
 * @author Pavlo Panchyshyn
 * @since 2019-03-31
 *
 */
public class Main {

  public static void main(String[] args) {
    Controller.execute();
  }
}
