package com.epam.lab.basic;

import java.util.ArrayList;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Class describes the model and implements the business logic of the learning task.
 *
 * @version 1.0
 * @author Pavlo Panchyshyn
 * @since 2019-03-31
 *
 */
public final class Model {

  @Contract
  private static boolean checkIsItEven(int integer) {
    boolean isEven = false;
    if (integer % 2 == 0) {
      isEven = true;
    }
    return isEven;
  }

  public static ArrayList<Integer> getEvenNumbersList(
      @NotNull final int[] interval) {
    final ArrayList<Integer> evenNumbersList = new ArrayList();
    for (int i = interval[1]; i >= interval[0]; i--) {
      if (checkIsItEven(i)) {
        evenNumbersList.add(i);
      }
    }
    return evenNumbersList;
  }

  public static ArrayList<Integer> getOddNumbersList(
      @NotNull final int[] interval) {
    final ArrayList<Integer> oddNumbersList = new ArrayList();
    for (int i = interval[0]; i <= interval[1]; i++) {
      if (!checkIsItEven(i)) {
        oddNumbersList.add(i);
      }
    }
    return oddNumbersList;
  }

  @Contract
  public static int getSum(@NotNull final ArrayList<Integer> list) {
    int sum = 0;
    for (int i : list) {
      sum += i;
    }
    return sum;
  }

  public static ArrayList<Integer> getFibonacci(
      int member1, int member2, int setSize) {
    ArrayList<Integer> fibonacci = new ArrayList();
    int temp = 0;
    if(member1 > member2) {
      temp = member1;
      member1 = member2;
      member2 = temp;
    }
    fibonacci.add(member1);
    fibonacci.add(member2);
    for (int i = 2; i < setSize; i++) {
      temp = member2;
      member2 += member1;
      member1 = temp;
      fibonacci.add(member2);
    }
    return fibonacci;
  }

  public static double getEvenPercentage(
      @NotNull final ArrayList<Integer> list) {
    double evenPercentage;
    int count = 0;
    for (int i : list) {
      if (checkIsItEven(i)) {
        count++;
      }
    }
    evenPercentage = (double) count / list.size()
        * Constants.ONE_HUNDRED_PERCENT;
    return evenPercentage;
  }

  public static double getOddPercentage(
      @NotNull final ArrayList<Integer> list) {
    double oddPercentage;
    int count = 0;
    for (int i : list) {
      if (!checkIsItEven(i)) {
        count++;
      }
    }
    oddPercentage = (double) count / list.size()
        * Constants.ONE_HUNDRED_PERCENT;
    return oddPercentage;
  }
}