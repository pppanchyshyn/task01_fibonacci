package com.epam.lab.basic;

import java.util.ArrayList;
import org.jetbrains.annotations.NotNull;

/**
 * Class implements the methods of information output to the user.
 *
 * @version 1.0
 * @author Pavlo Panchyshyn
 * @since 2019-03-31
 *
 */
public final class View {

  static void promptEnterInterval() {
    System.out.print(
        "Welcome to the study program. Please follow the prompts."
            + "\nEnter interval (for example: [1;100]): ");
  }

  static void promptEnterSetSize() {
    System.out.print("Please enter the size of set of Fibonacci numbers: ");
  }

  public static void printMenu() {
    System.out.println("Menu"
        + "\nTo get odd numbers from start to the end of interval press 1"
        + "\nTo get even numbers from end to the start of interval press 2"
        + "\nTo get the sum of odd and even numbers press 3"
        + "\nTo get Fibonacci numbers(F1 - the biggest odd number, "
        + "F2 – the biggest even number)"
        + " with specified size of set press 4"
        + "\nTo get percentage of odd and even Fibonacci numbers press 5"
        + "\nTo restart program press 6"
        + "\nTo exit press 7");
  }

  private static void printList(@NotNull final ArrayList<Integer> list) {
    int count = 0;
    for (int i : list) {
      System.out.printf("%-5d", i);
      count++;
      if (count % Constants.NUMBERS_PER_LINE == 0) {
        System.out.print("\n");
      }
    }
  }

  static void printOddNumbersList(final ArrayList<Integer> list) {
    System.out.println("Odd numbers from start to the end interval: ");
    printList(list);
    System.out.print("\nEnter your choice: ");
  }

  static void printEvenNumbersList(final ArrayList<Integer> list) {
    System.out.println("Even numbers from start to the end interval: ");
    printList(list);
    System.out.print("\nEnter your choice: ");
  }

  static void printSum(final ArrayList<Integer> oddNumbersList,
      final ArrayList<Integer> evenNumbersList) {
    System.out.print("Sum of odd numbers is " + Model.getSum(oddNumbersList)
        + "\nSum of even numbers is " + Model.getSum(evenNumbersList));
    System.out.print("\nEnter your choice: ");
  }

  static void printFibonacci(final ArrayList<Integer> list) {
    System.out.println("Fibonacci numbers: ");
    printList(list);
    System.out.print("\nEnter your choice: ");
  }

  static void printPercentage(
      final double oddNumPercentage, final double evenNumPercentage) {
    System.out.print("Odd Fibonacci numbers takes " + oddNumPercentage + "%\n"
        + "Even Fibonacci numbers takes " + evenNumPercentage + "%");
    System.out.print("\nEnter your choice: ");
  }
}
