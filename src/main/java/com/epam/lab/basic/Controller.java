package com.epam.lab.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *Class provides a connection between the program and the user interface.
 *
 * @version 1.0
 * @author Pavlo Panchyshyn
 * @since 2019-03-31
 *
 */
public final class Controller {

  private static Scanner input = new Scanner(System.in);

  private static int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        System.out.print("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  public static int[] getInterval() {
    int[] interval = new int[2];
    interval[0] = enterInteger();
    interval[1] = enterInteger();
    if (interval[0] > interval[1]) {
      final int temp = interval[0];
      interval[0] = interval[1];
      interval[1] = temp;
    }
    return interval;
  }

  public static int enterSetSize() {
    int setSize;
    while (true) {
      setSize = enterInteger();
      if (setSize >= Constants.MIN_SET_SIZE) {
        break;
      } else {
        System.out.print("Minimum size of set must be 3. Try again: ");
        input.nextLine();
      }
    }
    return setSize;
  }

  public static int enterMenuItemNumber() {
    int menuItemNumber;
    while (true) {
      menuItemNumber = enterInteger();
      if (menuItemNumber > 0 && menuItemNumber <= Constants.RESTART_VALUE) {
        break;
      } else {
        System.out.print("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItemNumber;
  }

  public static void execute() {
    while (true) {
      View.promptEnterInterval();
      final int[] interval = getInterval();
      View.promptEnterSetSize();
      final int setSize = enterSetSize();
      final ArrayList<Integer> evenNumbersList = Model.getEvenNumbersList(interval);
      final ArrayList<Integer> oddNumbersList = Model.getOddNumbersList(interval);
      final ArrayList<Integer> fibonacci = Model
          .getFibonacci(Collections.max(evenNumbersList),
              Collections.max(oddNumbersList), setSize);
      View.printMenu();
      int rewindValue = 0;
      while (!(rewindValue == Constants.REWIND_VALUE)) {
        final int menuItemNumber = enterMenuItemNumber();
        switch (menuItemNumber) {
          case Constants.CHOICE_1:
            View.printOddNumbersList(oddNumbersList);
            break;
          case Constants.CHOICE_2:
            View.printEvenNumbersList(evenNumbersList);
            break;
          case Constants.CHOICE_3:
            View.printSum(oddNumbersList, evenNumbersList);
            break;
          case Constants.CHOICE_4:
            View.printFibonacci(fibonacci);
            break;
          case Constants.CHOICE_5:
            View.printPercentage(Model.getOddPercentage(fibonacci),
                Model.getEvenPercentage(fibonacci));
            break;
          case Constants.CHOICE_6:
            rewindValue = Constants.REWIND_VALUE;
            break;
          case Constants.CHOICE_7:
            System.exit(0);
            break;
        }
      }
    }
  }
}
